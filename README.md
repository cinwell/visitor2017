#####HBuild开发的移动应用，MUI与VUE的配合，开发非常迅速。因为目前功能简单，暂时没有观察到性能方面有什么问题。一些简单的企业应用直接用MUI开发就可以了，android与ios通用。

- VUE官网：https://cn.vuejs.org/
- MUI官网：http://www.dcloud.io/mui.html

下面是截图：
![输入图片说明](https://git.oschina.net/uploads/images/2017/0625/124314_484fc9e9_87848.png "在这里输入图片标题")
![输入图片说明](https://git.oschina.net/uploads/images/2017/0625/124322_ec870156_87848.png "在这里输入图片标题")
![输入图片说明](https://git.oschina.net/uploads/images/2017/0625/124333_9c21f4b4_87848.png "在这里输入图片标题")
![输入图片说明](https://git.oschina.net/uploads/images/2017/0625/124346_618bff30_87848.png "在这里输入图片标题")
![输入图片说明](https://git.oschina.net/uploads/images/2017/0625/124355_c0f5e166_87848.png "在这里输入图片标题")
![输入图片说明](https://git.oschina.net/uploads/images/2017/0625/124403_f53076be_87848.png "在这里输入图片标题")
![输入图片说明](https://git.oschina.net/uploads/images/2017/0625/124411_56f45c08_87848.png "在这里输入图片标题")
![输入图片说明](https://git.oschina.net/uploads/images/2017/0625/124421_c4fbef6b_87848.png "在这里输入图片标题")
![输入图片说明](https://git.oschina.net/uploads/images/2017/0625/124429_293d87f8_87848.png "在这里输入图片标题")
![输入图片说明](https://git.oschina.net/uploads/images/2017/0625/124438_d147eeb7_87848.png "在这里输入图片标题")
![输入图片说明](https://git.oschina.net/uploads/images/2017/0625/124447_8f1b4438_87848.png "在这里输入图片标题")
![输入图片说明](https://git.oschina.net/uploads/images/2017/0625/124456_ba904996_87848.png "在这里输入图片标题")
![输入图片说明](https://git.oschina.net/uploads/images/2017/0625/124505_48d8f503_87848.png "在这里输入图片标题")
![输入图片说明](https://git.oschina.net/uploads/images/2017/0625/124513_f3259e50_87848.png "在这里输入图片标题")